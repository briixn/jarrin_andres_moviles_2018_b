package com.example.samy.timefigheter

import android.content.IntentSender
import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.PersistableBundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    internal lateinit var scoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var tapMeButton: Button
    internal var score = 0
    internal var gameStarted = false
    internal lateinit var countDownTime: CountDownTimer
    internal val initialCountDown = 60000L
    internal var timeleft = 60
    internal val countDownInterval = 1000L

    internal val TAG=MainActivity::class.java.simpleName

    companion object {
        private val SCORE_KEY = "SCORE_KEY"
        private  val TIME_LEFT_KEY= "TIME_LEFT_KEY"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG, "onCreate called. Score is $score")
//connect views to variables

        scoreTextView = findViewById<TextView>(R.id.score_textView)
        timeLeftTextView = findViewById<TextView>(R.id.time_left_textView)
        tapMeButton = findViewById<Button>(R.id.tap_me_button)
        resetGame()
        tapMeButton.setOnClickListener { _ -> incrementScore() }

        if (savedInstanceState != null){
            score= savedInstanceState.getInt(SCORE_KEY)
            timeleft=savedInstanceState.getInt(TIME_LEFT_KEY)
            restoreGame()
        }else{
            resetGame()
        }

    }

    private fun resetGame() {
        score = 0
        timeleft = 60
        val gameScore = getString(R.string.your_score, Integer.toString(score))
        scoreTextView.text = gameScore

        val timeLeftText = getString(R.string.time_left_30, Integer.toString(timeleft))
        timeLeftTextView.text = timeLeftText

        countDownTime = object : CountDownTimer( initialCountDown , countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeleft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left_30, Integer.toString(timeleft))
            }

            override fun onFinish() {
                endGame()
            }
        }

        gameStarted = false

    }


    private fun incrementScore() {

        score++
        //val newScore = "Your Score : " + Integer.toString(score)
        val newScore = getString(R.string.your_Score, Integer.toString(score))
        scoreTextView.text = newScore
        if (!gameStarted) {
            startGame()
        }
    }

    private fun startGame() {
        gameStarted = true
        countDownTime.start()
    }

    private fun endGame(){

        Toast.makeText(this, getString(R.string.game_over_message,Integer.toString(score)), Toast.LENGTH_LONG).show()
        resetGame()
    }

    private fun restoreGame(){
         val restoredScore=getString(R.string.your_score, Integer.toString(score))
         scoreTextView.text=restoredScore

         val restoredTime= getString(R.string.time_left_30, Integer.toString(timeleft))
         timeLeftTextView.text=restoredTime

         countDownTime = object : CountDownTimer(timeleft*1000L ,countDownInterval){

             override fun onFinish() {
                 endGame()
             }

             override fun onTick(millisUntilFinished: Long) {
                 timeleft=millisUntilFinished.toInt()/ 1000

                 timeLeftTextView.text=getString(R.string.time_left_30,timeleft.toString())


             }


         }

        countDownTime.start()
     }
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putInt(SCORE_KEY,score)
        outState.putInt(TIME_LEFT_KEY,timeleft)
        countDownTime.cancel()
        Log.d(TAG, "onSaveInstance State: score =$score &timeleft =$timeleft ")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, " onDestroy Called")
    }
    }


