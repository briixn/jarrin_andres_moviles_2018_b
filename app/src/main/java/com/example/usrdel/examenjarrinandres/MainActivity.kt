package com.example.usrdel.examenjarrinandres

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    internal lateinit var empezarBoton:Button
    internal lateinit var tiempoBoton:Button
    internal lateinit var puntajeTextView: TextView
    internal lateinit var tiempoextView: TextView

    internal var puntaje=0
    internal var empezo=false

    internal lateinit var cuentaAtras:CountDownTimer
    internal val cuentaInicial = 10000L
    internal var timeleft = 10
    internal val countDownInterval = 1000L
    internal var n =0
    internal var tiempoActual = 10

    internal val TAG= MainActivity::class.java.simpleName

    companion object {
        private val PUNTAJE_KEY = "PUNTAJE_KEY"
        private  val TIME_LEFT_KEY= "TIME_LEFT_KEY"
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        n = (0..10).shuffled().first()
        println("on create n=")
        empezarBoton = findViewById<Button>(R.id.startButton)
        tiempoBoton = findViewById<Button>(R.id.tiempoButton)
        puntajeTextView = findViewById<TextView>(R.id.puntajeTextView)
        tiempoextView = findViewById<TextView>(R.id.tiempoTextView)

        //resetGame()

        empezarBoton.setOnClickListener{_ -> startGame()}
        tiempoBoton.setOnClickListener(){
            if((10-n)==endGame())
                incrementScore(100)
            if(((10-n)==endGame()+1)||((10-n)==endGame()-1))
                incrementScore(50)
            else
                endGame()

        }
        if (savedInstanceState != null){
            puntaje= savedInstanceState.getInt(PUNTAJE_KEY)
            timeleft=savedInstanceState.getInt(TIME_LEFT_KEY)
            restoreGame()
        }else{
            resetGame()
        }

       // tiempoBoton.setOnClickListener{_ -> endGame()}

        if (savedInstanceState != null){
            puntaje= savedInstanceState.getInt(PUNTAJE_KEY)
            timeleft=savedInstanceState.getInt(TIME_LEFT_KEY)
            restoreGame()
        }else{
            resetGame()
        }

    }

    private fun endGame():Int{
        println("end game n=${cuentaAtras.toString()}")

        Toast.makeText(this, "FIN", Toast.LENGTH_LONG).show()
        //resetGame()
        tiempoActual=timeleft
        return tiempoActual
    }

    private fun resetGame() {
        println("reset game n")

        puntaje = 0
        timeleft = 10
        val gameScore = puntaje
        puntajeTextView.text = "$gameScore"

        val timeLeftText = getString(R.string.tiempo_adivinar)+" $timeleft"
        tiempoextView.text = "$n"

        cuentaAtras = object : CountDownTimer( cuentaInicial , countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeleft = millisUntilFinished.toInt() / 1000
                tiempoextView.text = "$n"
            }

            override fun onFinish() {
                println("on finish ")

                Log.d(TAG, " fin")

            }
        }

        empezo = false

    }

    private fun incrementScore( a:Int) {
        println("incrementando ")

        puntaje=a
        //val newScore = "Your Score : " + Integer.toString(score)
       // val newScore = getString(R.string.puntos, Integer.toString(puntaje))

        //val newScore = getString(puntaje)
        puntajeTextView.text = "$puntaje"
        if (!empezo) {
            startGame()
        }
    }

    private fun startGame() {
        println("start game ")

        empezo = true
        cuentaAtras.start()
    }



    private fun restoreGame(){
        println("restore game ")

        // val restoredScore=getString(R.string.puntos, Integer.toString(puntaje))
        val restoredScore=getString(R.string.puntos)+"$puntaje"
        puntajeTextView.text=restoredScore

        // val restoredTime= getString(R.string.tiempo_adivinar, Integer.toString(timeleft))
        val restoredTime= getString(R.string.tiempo_adivinar)
        tiempoBoton.text=restoredTime

        cuentaAtras = object : CountDownTimer(timeleft*1000L ,countDownInterval){

            override fun onFinish() {
                println("on finish n")

                (        Log.d(TAG, " fin Called"))

            }

            override fun onTick(millisUntilFinished: Long) {
                timeleft=millisUntilFinished.toInt()/ 1000

                tiempoextView.text="$n"


            }


        }

        cuentaAtras.start()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putInt(PUNTAJE_KEY,puntaje)
        outState.putInt(TIME_LEFT_KEY,timeleft)
        cuentaAtras.cancel()
        Log.d(TAG, "onSaveInstance State: score =$puntaje &timeleft =$timeleft ")
    }

    override fun onDestroy() {
        println("on Destroy ")

        super.onDestroy()
        Log.d(TAG, " onDestroy Called")
    }
}
